﻿using System;
using System.Threading.Tasks;

namespace AoC_2019
{
    public class IntCodeComputer
    {
        private int[] _codes;
        private Amplifier _input;
        private Amplifier _output;
        private static int highestThrust = 0;

        public IntCodeComputer(int[] codes, Amplifier input, Amplifier output)
        {
            _codes = codes;
            _input = input;
            _output = output;
        }

        public async Task<int> Run()
        {
            var step = 0;
            for (var i = 0; i < _codes.Length; i += step)
            {
                if (_codes[i] % 100 == 1)
                {

                    OpCodeAdd(_codes, i, _codes[i] / 100 % 10, _codes[i] / 1000 % 10);
                    step = 4;
                }
                else if (_codes[i] % 100 == 2)
                {
                    OpCodeMultiply(_codes, i, _codes[i] / 100 % 10, _codes[i] / 1000 % 10);
                    step = 4;
                }
                else if (_codes[i] % 100 == 3)
                {
                    while (_input.GetInput().Count == 0)
                    {
                        await Task.Delay(10).ConfigureAwait(false);
                    }
                    OpCodeInput(_codes, i, _input.GetInput().Dequeue());
                    step = 2;
                }
                else if (_codes[i] % 100 == 4)
                {
                    _output.QueueInput(OpCodeOutput(_codes, i));
                    step = 2;
                }
                else if (_codes[i] % 100 == 5)
                {
                    i = OpCodeJumpIfTrue(_codes, i, _codes[i] / 100 % 10, _codes[i] / 1000 % 10);
                    step = 0;
                }
                else if (_codes[i] % 100 == 6)
                {
                    i = OpCodeJumpIfFalse(_codes, i, _codes[i] / 100 % 10, _codes[i] / 1000 % 10);
                    step = 0;
                }
                else if (_codes[i] % 100 == 7)
                {
                    OpCodeLessThan(_codes, i, _codes[i] / 100 % 10, _codes[i] / 1000 % 10, _codes[i] / 10000 % 10);
                    step = 4;
                }
                else if (_codes[i] % 100 == 8)
                {
                    OpCodeEquals(_codes, i, _codes[i] / 100 % 10, _codes[i] / 1000 % 10, _codes[i] / 10000 % 10);
                    step = 4;
                }
                else if (_codes[i] % 100 == 99)
                {
                    //Console.WriteLine("Halt.");
                    break;
                }
                else
                {
                    throw new Exception($"Invalid op code, {_codes[i]}, read at position {i}.");
                }
            }

            return _codes[0];
        }

        private static void OpCodeMultiply(int[] codes, int index, int firstParameterMode, int secondParameterMode)
        {
            codes[codes[index + 3]] = (firstParameterMode == 0 ? codes[codes[index + 1]] : codes[index + 1]) * (secondParameterMode == 0 ? codes[codes[index + 2]] : codes[index + 2]);
        }

        private static void OpCodeAdd(int[] codes, int index, int firstParameterMode, int secondParameterMode)
        {
            codes[codes[index + 3]] = (firstParameterMode == 0 ? codes[codes[index + 1]] : codes[index + 1]) + (secondParameterMode == 0 ? codes[codes[index + 2]] : codes[index + 2]);
        }

        private static void OpCodeInput(int[] codes, int index, int input)
        {
            codes[codes[index + 1]] = input;
        }

        private static int OpCodeOutput(int[] codes, int index)
        {
            var output = codes[codes[index + 1]];
            if (output > highestThrust)
            {
                highestThrust = output;
                Console.WriteLine(highestThrust);
            }
            
            return output;
        }

        private static int OpCodeJumpIfTrue(int[] codes, int index, int firstParameterMode, int secondParameterMode)
        {
            if ((firstParameterMode == 0 ? codes[codes[index + 1]] : codes[index + 1]) != 0)
            {
                return secondParameterMode == 0 ? codes[codes[index + 2]] : codes[index + 2];
            }

            return index + 3;
        }

        private static int OpCodeJumpIfFalse(int[] codes, int index, int firstParameterMode, int secondParameterMode)
        {
            if ((firstParameterMode == 0 ? codes[codes[index + 1]] : codes[index + 1]) == 0)
            {
                return secondParameterMode == 0 ? codes[codes[index + 2]] : codes[index + 2];
            }

            return index + 3;
        }

        private static void OpCodeLessThan(int[] codes, int index, int firstParameterMode, int secondParameterMode, int thirdParameterMode)
        {
            codes[thirdParameterMode == 0 ? codes[index + 3] : index + 3] = (firstParameterMode == 0 ? codes[codes[index + 1]] : codes[index + 1]) < (secondParameterMode == 0 ? codes[codes[index + 2]] : codes[index + 2]) ? 1 : 0;
        }

        private static void OpCodeEquals(int[] codes, int index, int firstParameterMode, int secondParameterMode, int thirdParameterMode)
        {
            codes[thirdParameterMode == 0 ? codes[index + 3] : index + 3] = (firstParameterMode == 0 ? codes[codes[index + 1]] : codes[index + 1]) == (secondParameterMode == 0 ? codes[codes[index + 2]] : codes[index + 2]) ? 1 : 0;
        }
    }
}