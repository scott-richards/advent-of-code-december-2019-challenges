﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AoC_2019
{
    using System.IO;
    using System;

    public class Amplifier
    {
        private Amplifier _output;
        private readonly Queue<int> input = new Queue<int>();
        private int[] _codes;
        private IntCodeComputer intCodeComputer;

        public Amplifier(int[] codes, int phaseSetting)
        {
            input.Enqueue(phaseSetting);
            _codes = codes.BlockCopy();
        }

        public void SetAmplifierOutput(Amplifier output)
        {
            _output = output;
        }

        public void QueueInput(int x)
        {
            input.Enqueue(x);
        }

        public Queue<int> GetInput()
        {
            return input;
        }

        public void RunProgram()
        {
            intCodeComputer = new IntCodeComputer(_codes, this, _output);
            _ = intCodeComputer.Run();
        }
    }

    class Program
    {
        private static int highestThrust = 0;

        private static void Main(string[] args)
        {
            var codes = Array.ConvertAll(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt")).Split(','), int.Parse);
            var permutations = new[] {5, 6, 7, 8, 9}.GetPermutations(5).ToList();
            foreach (var permutation in permutations)
            {
                var currentPermutation = permutation.ToArray();
                var ampA = new Amplifier(codes, currentPermutation[0]);
                var ampB = new Amplifier(codes, currentPermutation[1]);
                var ampC = new Amplifier(codes, currentPermutation[2]);
                var ampD = new Amplifier(codes, currentPermutation[3]);
                var ampE = new Amplifier(codes, currentPermutation[4]);
                ampA.SetAmplifierOutput(ampB);
                ampB.SetAmplifierOutput(ampC);
                ampC.SetAmplifierOutput(ampD);
                ampD.SetAmplifierOutput(ampE);
                ampE.SetAmplifierOutput(ampA);
                ampA.QueueInput(0);
                ampA.RunProgram();
                ampB.RunProgram();
                ampC.RunProgram();
                ampD.RunProgram();
                ampE.RunProgram();
            }
            Console.WriteLine(highestThrust);
            Console.ReadKey();
        }
    }

    public static class ExtensionMethods
    {
        public static int[] BlockCopy(this int[] array)
        {
            var copy = new int[array.Length];
            Buffer.BlockCopy(array, 0, copy, 0, array.Length * sizeof(int));
            return copy;
        }

        public static IEnumerable<IEnumerable<T>> GetPermutations<T>(this IEnumerable<T> list, int length)
        {
            if (length == 1)
            {
                return list.Select(t => new T[] { t });
            }

            return GetPermutations(list, length - 1).SelectMany(t => list.Where(e => !t.Contains(e)), (t1, t2) => t1.Concat(new T[] { t2 }));
        }
    }
}