﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace AoC_2019
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.CursorVisible = false;
            var codes = new Hashtable();
            var program = Array.ConvertAll(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt")).Split(','), long.Parse);
            for (var i = 0; i < program.Length; i++)
            {
                codes.Add(i, program[i]);
            }

            new ArcadeGame(codes);
            Console.ReadKey();
        }
    }

    public class ArcadeGame : IInput, IOutput, IHalt
    {
        private Dictionary<(int, int), int> _tiles = new Dictionary<(int, int), int>();
        private (int x, int y) _position = (0,0);
        private (int x, int y) _attemptedNewPosition;

        public ArcadeGame(Hashtable paintProgram)
        {
            var intCodeComputer = new IntCodeComputer(paintProgram, this, this, this);
            intCodeComputer.Run();
            DrawBoardState();
        }

        public void PutOutput(long output)
        {
            if (!_tiles.ContainsKey(_attemptedNewPosition))
            {
                _tiles.Add(_attemptedNewPosition, (int) output);
            }
            else
            {
                _tiles[_attemptedNewPosition] = (int) output;
            }

            if ((int) output == 1 || (int) output == 2)
            {
                _position = _attemptedNewPosition;
            }
        }

        public void Halt()
        {
            DrawBoardState();
        }

        public int GetInput()
        {
            DrawBoardState();
            return GetJoystickInput(Console.ReadKey());
        }

        private int GetJoystickInput(ConsoleKeyInfo key)
        {
            _attemptedNewPosition = _position;
            switch (key.Key)
            {
                case ConsoleKey.LeftArrow:
                {
                    _attemptedNewPosition.x--;
                    return 3;
                }

                case ConsoleKey.RightArrow:
                {
                    _attemptedNewPosition.x++;
                    return 4;
                }

                case ConsoleKey.UpArrow:
                {
                    _attemptedNewPosition.y++;
                    return 1;
                }

                case ConsoleKey.DownArrow:
                {
                    _attemptedNewPosition.y--;
                    return 2;
                }

                default:
                    _attemptedNewPosition.y++;
                    return 1;
            }
        }

        private void DrawBoardState()
        {
            Console.Clear();
            (int x, int y) min = (_position.x - 50, _position.y - 10);
            (int x, int y) max = (_position.x + 50, _position.y + 10);
            for (var y = max.y; y >= min.y; y--)
            {
                for (var x = min.x; x <= max.x; x++)
                {
                    if (_position.Equals((x, y)))
                    {
                        Console.Write(DrawTile(3));
                    }
                    else if (_tiles.ContainsKey((x, y)))
                    {
                        Console.Write(DrawTile(_tiles[(x, y)]));
                    }
                    else
                    {
                        Console.Write(DrawTile(4));
                    }
                }
                Console.WriteLine();
            }
        }

        private char DrawTile(int tile)
        {
            switch (tile)
            {
                case 0:
                    return '#';
                case 1:
                    return '.';
                case 2:
                    return 'O';
                case 3:
                    return 'D';
                case 4:
                    return ' ';
                default:
                    throw new Exception();
            }
        }
    }

    public interface IOutput
    {
        void PutOutput(long output);
    }

    public interface IInput
    {
        int GetInput();
    }

    public interface IHalt
    {
        void Halt();
    }
}