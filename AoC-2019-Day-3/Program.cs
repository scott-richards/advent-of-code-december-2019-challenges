﻿namespace AoC_2019_Day_3a
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Numerics;
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            var wires = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "wires.txt"));
            var wireOne = CreateWire(wires[0].Split(','));
            var wireTwo = CreateWire(wires[1].Split(','));
            var closestDistance = 0;
            var lowestStepIntersectionSum = 0;
            for (var x = 0; x < wireOne.GetLines().Count; x++)
            {
                for (var y = 0; y < wireTwo.GetLines().Count; y++)
                {
                    if (x == 0 && y == 0)
                    {
                        continue;
                    }
                    FindIntersection(wireOne.GetLines()[x].Start, wireOne.GetLines()[x].End, wireTwo.GetLines()[y].Start, wireTwo.GetLines()[y].End, out var linesIntersect, out var segmentIntersects, out var intersection, out var closeP1, out var closeP2);
                    if (segmentIntersects)
                    {
                        if (closestDistance == 0 || closestDistance > MathF.Abs(intersection.X) + MathF.Abs(intersection.Y))
                        {
                            closestDistance = (int) MathF.Round(MathF.Abs(intersection.X) + MathF.Abs(intersection.Y));
                        }

                        var currentStepIntersectionSum = GetStepDistance(wireOne.GetLines().GetRange(0, x + 1), intersection) + GetStepDistance(wireTwo.GetLines().GetRange(0, y + 1), intersection);
                        if (lowestStepIntersectionSum == 0 || lowestStepIntersectionSum > currentStepIntersectionSum)
                        {
                            lowestStepIntersectionSum = currentStepIntersectionSum;
                        }
                    }
                }
            }
            Console.WriteLine($"Closest Distance: {closestDistance}");
            Console.WriteLine($"Closest Intersection: {lowestStepIntersectionSum}");
            Console.ReadLine();
        }

        private static int GetStepDistance(List<Line> lines, Vector2 intersection)
        {
            var stepDistance = 0;
            for (var i = 0; i < lines.Count - 1; i++)
            {
                if ((int) lines[i].Start.X == (int) lines[i].End.X)
                {
                    stepDistance += (int) MathF.Abs(lines[i].Start.Y - lines[i].End.Y);
                }
                else
                {
                    stepDistance += (int) MathF.Abs(lines[i].Start.X - lines[i].End.X);
                }
            }
            
            if ((int) lines[lines.Count - 1].Start.X == (int) lines[lines.Count - 1].End.X)
            {
                stepDistance += (int) MathF.Abs(lines[lines.Count - 1].Start.Y - intersection.Y);
            }
            else
            {
                stepDistance += (int) MathF.Abs(lines[lines.Count - 1].Start.X - intersection.X);
            }

            return stepDistance;
        }

        private static Wire CreateWire(string[] path)
        {
            var lines = new List<Line>();
            for (var i = 0; i < path.Length; i++)
            {
                if (int.TryParse(path[i].Substring(1), out var step))
                {
                    switch (path[i][0])
                    {
                        case 'R' when i == 0:
                            lines.Add(new Line {Start = Vector2.Zero, End = new Vector2(step, 0)});
                            break;
                        case 'R':
                            lines.Add(new Line {Start = lines.Last().End, End = new Vector2(lines.Last().End.X + step, lines.Last().End.Y)});
                            break;
                        case 'L' when i == 0:
                            lines.Add(new Line {Start = Vector2.Zero, End = new Vector2(-step, 0)});
                            break;
                        case 'L':
                            lines.Add(new Line {Start = lines.Last().End, End = new Vector2(lines.Last().End.X - step, lines.Last().End.Y)});
                            break;
                        case 'U' when i == 0:
                            lines.Add(new Line {Start = Vector2.Zero, End = new Vector2(0, step)});
                            break;
                        case 'U':
                            lines.Add(new Line {Start = lines.Last().End, End = new Vector2(lines.Last().End.X, lines.Last().End.Y + step)});
                            break;
                        case 'D' when i == 0:
                            lines.Add(new Line {Start = Vector2.Zero, End = new Vector2(0, -step)});
                            break;
                        case 'D':
                            lines.Add(new Line {Start = lines.Last().End, End = new Vector2(lines.Last().End.X, lines.Last().End.Y - step)});
                            break;
                        default:
                            throw new Exception("[Default Switch Case] You really shouldn't be here, and now your ship has probably exploded.");
                    }
                }
                else
                {
                    throw new Exception("[Failed int TryParse] You really shouldn't be here, and now your ship has probably exploded.");
                }
            }

            return new Wire(lines);
        }

        private static void FindIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, out bool linesIntersect, out bool segmentsIntersect, out Vector2 intersection, out Vector2 closeP1, out Vector2 closeP2)
        {
            // Get the segments' parameters.
            var dx12 = p2.X - p1.X;
            var dy12 = p2.Y - p1.Y;
            var dx34 = p4.X - p3.X;
            var dy34 = p4.Y - p3.Y;

            // Solve for t1 and t2
            var denominator = dy12 * dx34 - dx12 * dy34;
            var t1 = ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34) / denominator;
            if (float.IsInfinity(t1))
            {
                // The lines are parallel (or close enough to it).
                linesIntersect = false;
                segmentsIntersect = false;
                intersection = new Vector2(float.NaN, float.NaN);
                closeP1 = new Vector2(float.NaN, float.NaN);
                closeP2 = new Vector2(float.NaN, float.NaN);
                return;
            }
            linesIntersect = true;

            var t2 = ((p3.X - p1.X) * dy12 + (p1.Y - p3.Y) * dx12) / -denominator;
            intersection = new Vector2(p1.X + dx12 * t1, p1.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segmentsIntersect = t1 >= 0 && t1 <= 1 && t2 >= 0 && t2 <= 1;

            // Find the closest points on the segments.
            if (t1 < 0)
            {
                t1 = 0;
            }
            else if (t1 > 1)
            {
                t1 = 1;
            }

            if (t2 < 0)
            {
                t2 = 0;
            }
            else if (t2 > 1)
            {
                t2 = 1;
            }

            closeP1 = new Vector2(p1.X + dx12 * t1, p1.Y + dy12 * t1);
            closeP2 = new Vector2(p3.X + dx34 * t2, p3.Y + dy34 * t2);
        }
    }


    public struct Line
    {
        public Vector2 Start;
        public Vector2 End;
    }

    public class Wire
    {
        private List<Line> _lines;
        public Wire(List<Line> lines)
        {
            _lines = lines;
        }

        public List<Line> GetLines()
        {
            return _lines;
        }
    }
}

/*--- Day 3: Crossed Wires ---
The gravity assist was successful, and you're well on your way to the Venus refuelling station. During the rush back on Earth, the fuel management system wasn't completely installed, so that's next on the priority list.

Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a central port and extend outward on a grid. You trace the path each wire takes as it leaves the central port, one wire per line of text (your puzzle input).

The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you need to find the intersection point closest to the central port. Because the wires are on a grid, use the Manhattan distance for this measurement. While the wires do technically cross right at the central port where they both start, this point does not count, nor does a wire count as crossing with itself.

For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o), it goes right 8, up 5, left 5, and finally down 3:

...........
...........
...........
....+----+.
....|....|.
....|....|.
....|....|.
.........|.
.o-------+.
...........
Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........
These wires cross at two locations (marked X), but the lower-left one is closer to the central port: its distance is 3 + 3 = 6.

Here are a few more examples:

R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83 = distance 159
R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135
What is the Manhattan distance from the central port to the closest intersection?

Your puzzle answer was 293.

--- Part Two ---
It turns out that this circuit is very timing-sensitive; you actually need to minimize the signal delay.

To do this, calculate the number of steps each wire takes to reach each intersection; choose the intersection where the sum of both wires' steps is lowest. If a wire visits a position on the grid multiple times, use the steps value from the first time it visits that position when calculating the total value of a specific intersection.

The number of steps a wire takes is the total number of grid squares the wire has entered to get to that location, including the intersection being considered. Again consider the example from above:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........
In the above example, the intersection closest to the central port is reached after 8+5+5+2 = 20 steps by the first wire and 7+6+4+3 = 20 steps by the second wire for a total of 20+20 = 40 steps.

However, the top-right intersection is better: the first wire takes only 8+5+2 = 15 and the second wire takes only 7+6+2 = 15, a total of 15+15 = 30 steps.

Here are the best steps for the extra examples from above:

R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83 = 610 steps
R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = 410 steps
What is the fewest combined steps the wires must take to reach an intersection?

Your puzzle answer was 27306.
*/