﻿namespace AoC_2019
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.IO;
    using System;

    class Program
    {
        private static void Main(string[] args)
        {
            var codes = new Hashtable();
            var program = Array.ConvertAll(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt")).Split(','), long.Parse);
            for (var i = 0; i < program.Length; i++)
            {
                codes.Add(i, program[i]);
            }
            new PaintBot(codes);
            Console.ReadKey();
        }
    }

    public class PaintBot : IInput, IOutput, IHalt
    {
        private (int x, int y) _position = (0,0);
        private Dictionary<(int x, int y), int> _paintGrid = new Dictionary<(int x, int y), int>();
        private bool _isMoveInstruction;

        private enum Directions
        {
            Up,
            Down,
            Left,
            Right
        }

        private Directions _facingDirection = Directions.Up;

        public PaintBot(Hashtable paintProgram)
        {
            _paintGrid.Add(_position, 1);
            var intCodeComputer = new IntCodeComputer(paintProgram, this, this, this);
            _ = intCodeComputer.Run();
        }

        public async Task<int> GetInput()
        {
            if (!_paintGrid.ContainsKey(_position))
            {
                _paintGrid.Add(_position, 0);
            }
            return _paintGrid[_position];
        }

        public void PutOutput(long output)
        {
            if (_isMoveInstruction)
            {
                Turn((int) output);
            }
            else
            {
                Paint((int) output);
            }

            _isMoveInstruction = !_isMoveInstruction;
        }

        private void Turn(int direction)
        {
            switch (_facingDirection)
            {
                case Directions.Up:
                    _facingDirection = direction == 0 ? Directions.Left : Directions.Right;
                    break;
                case Directions.Down:
                    _facingDirection = direction == 0 ? Directions.Right : Directions.Left;
                    break;
                case Directions.Left:
                    _facingDirection = direction == 0 ? Directions.Down : Directions.Up;
                    break;
                case Directions.Right:
                    _facingDirection = direction == 0 ? Directions.Up : Directions.Down;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Move();
        }

        private void Move()
        {
            switch (_facingDirection)
            {
                case Directions.Up:
                    _position.y += 1;
                    break;
                case Directions.Down:
                    _position.y -= 1;
                    break;
                case Directions.Left:
                    _position.x -= 1;
                    break;
                case Directions.Right:
                    _position.x += 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Paint(int color)
        {
            _paintGrid[_position] = color;
        }

        public void Halt()
        {
            var xMax = 0;
            var yMax = 0;
            var xMin = 0;
            var yMin = 0;
            foreach (var ((x, y), _) in _paintGrid)
            {
                xMin = xMin > x ? x : xMin;
                yMin = yMin > y ? y : yMin;
                yMax = yMax < y ? y : yMax;
                xMax = xMax < x ? x : xMax;
            }
            for (var y = yMax; y >= yMin; y--)
            {
                for (var x = xMin; x <= xMax; x++)
                {
                    if (_paintGrid.ContainsKey((x, y)))
                    {
                        Console.Write(_paintGrid[(x,y)] == 0 ? ' ' : '#');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
        }
    }

    public interface IOutput
    {
        void PutOutput(long output);
    }

    public interface IInput
    {
        Task<int> GetInput();
    }

    public interface IHalt
    {
        void Halt();
    }
}