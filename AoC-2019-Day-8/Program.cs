﻿namespace AoC_2019_Day_8
{
    using System;
    using System.IO;
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            var wide = 25;
            var tall = 6;
            var codeString = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt"));
            var codes = new int[codeString.Length];
            for (var i = 0; i < codeString.Length; i++)
            {
                codes[i] = (int) char.GetNumericValue(codeString[i]);
            }
            var image = new Image(wide, tall, codes);
            image.Print();
        }


    }

    public class Image
    {
        private readonly int _wide;
        private readonly int _tall;
        private readonly int _layerSize;
        private readonly int[] _pixelCodes;
        private readonly Layer[] _layers;
        private Layer _flattenedImage;

        public Image(int wide, int tall, int[] pixelCodes)
        {
            _wide = wide;
            _tall = tall;
            _layerSize = _wide * _tall;
            _pixelCodes = pixelCodes;
            _layers = new Layer[_pixelCodes.Length / _layerSize];
            BuildLayers();
            FlattenImage();
        }

        private void BuildLayers()
        {
            var currentLayer = 0;
            for (var i = 0; i < _pixelCodes.Length; i += _layerSize)
            {
                var layer = new Layer(_wide, _tall, _pixelCodes.Slice(i, i + _layerSize), currentLayer);
                _layers[currentLayer] = layer;
                currentLayer++;
            }
        }

        private void FlattenImage()
        {
            var flattenedImageCodes = new int[_wide, _tall];
            for (var y = 0; y < flattenedImageCodes.GetLength(1); y++)
            {
                for (var x = 0; x < flattenedImageCodes.GetLength(0); x++)
                {
                    flattenedImageCodes[x, y] = 2;
                }
            }
            for (var i = 0; i < _layers.Length; i++)
            {
                for (var y = 0; y < flattenedImageCodes.GetLength(1); y++)
                {
                    for (var x = 0; x < flattenedImageCodes.GetLength(0); x++)
                    {
                        if (flattenedImageCodes[x, y] == 2 && _layers[i].GetPixel(x, y) < 2)
                        {
                            flattenedImageCodes[x, y] = _layers[i].GetPixel(x, y);
                        }
                    }
                }
            }
            _flattenedImage = new Layer(flattenedImageCodes);
        }

        public Layer GetLayerWithFewestOccurrences(int number)
        {
            var occurrences = new int[_layers.Length];
            for (var i = 0; i < _layers.Length; i++)
            {
                occurrences[i] = _layers[i].GetNumberOfOccurrences(number);
            }

            return _layers[Array.IndexOf(occurrences, occurrences.Min())];
        }

        public void Print()
        {
            _flattenedImage.PrintLayer();
        }
    }

    public class Layer
    {
        private readonly int _layerIndex;
        private readonly int[,] _layer;

        public Layer(int wide, int tall, int[] layerPixelCodes, int layerIndex)
        {
            _layer = new int[wide,tall];
            _layerIndex = layerIndex;
            BuildLayer(layerPixelCodes);
        }

        public Layer(int[,] layer)
        {
            _layer = layer;
        }

        private void BuildLayer(int[] layerPixelCodes)
        {
            var index = 0;
            for (var y = 0; y < _layer.GetLength(1); y++)
            {
                for (var x = 0; x < _layer.GetLength(0); x++)
                {
                    _layer[x, y] = layerPixelCodes[index];
                    index++;
                }
            }
        }

        public int GetNumberOfOccurrences(int numberToMatch)
        {
            var occurrences = 0;
            for (var y = 0; y < _layer.GetLength(1); y++)
            {
                for (var x = 0; x < _layer.GetLength(0); x++)
                {
                    if (_layer[x, y] == numberToMatch)
                    {
                        occurrences++;
                    }
                }
            }

            return occurrences;
        }

        public int MultiplyNumberOfOccurrences(int numberOne, int numberTwo)
        {
            return GetNumberOfOccurrences(numberOne) * GetNumberOfOccurrences(numberTwo);
        }

        public int GetLayerIndex()
        {
            return _layerIndex;
        }

        public int GetPixel(int x, int y)
        {
            return _layer[x, y];
        }

        public void PrintLayer()
        {
            for (var y = 0; y < _layer.GetLength(1); y++)
            {
                for (var x = 0; x < _layer.GetLength(0); x++)
                {
                    Console.Write(_layer[x, y] == 1 ? "#" : " ");
                }
                Console.WriteLine();
            }
        }
    }

    public static class Extensions
    {
        public static T[] Slice<T>(this T[] source, int startInclusive, int endExclusive)
        {
            if (endExclusive < 0)
            {
                endExclusive = source.Length + endExclusive;
            }
            var len = endExclusive - startInclusive;

            var res = new T[len];
            for (var i = 0; i < len; i++)
            {
                res[i] = source[i + startInclusive];
            }
            return res;
        }
    }
}
