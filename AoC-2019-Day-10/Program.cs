﻿namespace AoC_2019
{
    using System.Collections.Generic;
    using System.Linq;
    using System.IO;
    using System;

    class Program
    {
        private static (int, int) _maxBounds;
        private static ((int x, int y), int visibleAsteroids) _monitoringStation;
        private static void Main(string[] args)
        {
            var program = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt"));
            var asteroids = new List<(int,int)>();
            _maxBounds = (program[0].Trim().Length - 1, program.Length - 1);
            for (var y = 0; y < program.Length; y++)
            {
                for (var x = 0; x < program[y].Length; x++)
                {
                    if (program[y][x] == '#')
                    {
                        asteroids.Add((x,y));
                    }
                }
            }
            _monitoringStation = HighestVisibilityAsteroid(asteroids);
            MiningRotation(asteroids);
            Console.ReadKey();
        }

        private static void MiningRotation(List<(int, int)> asteroids)
        {
            var remainingAsteroids = new List<(int, int)>(asteroids);
            var visibleAsteroids = VisibleAsteroids(remainingAsteroids, _monitoringStation.Item1);
            var asteroidsMined = 0;
            while (visibleAsteroids.Count > 0)
            {
                visibleAsteroids = visibleAsteroids.OrderBy(x => _monitoringStation.Item1.Angle(x)).ToList();
                foreach (var destroyedAsteroid in visibleAsteroids)
                {
                    asteroidsMined++;
                    if (asteroidsMined == 200)
                    {
                        Console.WriteLine($"200th asteroid: {destroyedAsteroid}");
                        Console.WriteLine($"x * 100 + y: {destroyedAsteroid.Item1 * 100 + destroyedAsteroid.Item2}");
                    }
                    remainingAsteroids.Remove(destroyedAsteroid);
                }
                visibleAsteroids = VisibleAsteroids(remainingAsteroids, _monitoringStation.Item1);
            }
        }

        private static List<(int, int)> VisibleAsteroids(List<(int,int)> asteroids, (int, int) asteroidCheckingFrom)
        {
            var visibleAsteroids = new List<(int,int)>();
            var visibilityCheck = new List<(int, int)>(asteroids);
            if (visibilityCheck.Contains(asteroidCheckingFrom))
            {
                visibilityCheck.Remove(asteroidCheckingFrom);
            }
            visibilityCheck = visibilityCheck.OrderBy(x => x.DistanceTo(asteroidCheckingFrom)).ToList();
            while (visibilityCheck.Count > 0)
            {
                var visibleAsteroid = visibilityCheck[0];
                visibilityCheck.RemoveAt(0);
                visibleAsteroids.Add(visibleAsteroid);
                var step = (visibleAsteroid.Item1 - asteroidCheckingFrom.Item1, visibleAsteroid.Item2 - asteroidCheckingFrom.Item2);
                var gcd = step.GreatestCommonDenominator();
                step.Item1 = step.Item1 / gcd;
                step.Item2 = step.Item2 / gcd;
                var currentStepCheck = (visibleAsteroid.Item1 + step.Item1, visibleAsteroid.Item2 + step.Item2);
                while (currentStepCheck.Item1 <= _maxBounds.Item1 && currentStepCheck.Item1 >= 0 && currentStepCheck.Item2 <= _maxBounds.Item2 && currentStepCheck.Item2 >= 0 && visibilityCheck.Count != 0)
                {
                    if (visibilityCheck.Contains(currentStepCheck))
                    {
                        visibilityCheck.Remove(currentStepCheck);
                    }

                    currentStepCheck = (currentStepCheck.Item1 + step.Item1, currentStepCheck.Item2 + step.Item2);
                }
            }

            return visibleAsteroids;
        }

        private static ((int x, int y), int visibleAsteroids) HighestVisibilityAsteroid(List<(int, int)> asteroids)
        {
            var currentStationCandidate = ((0,0), 0);
            foreach (var asteroid in asteroids)
            {
                var visibleAsteroids = VisibleAsteroids(asteroids, asteroid).Count;
                if (visibleAsteroids > currentStationCandidate.Item2)
                {
                    currentStationCandidate.Item1 = asteroid;
                    currentStationCandidate.Item2 = visibleAsteroids;
                }
            }
            Console.WriteLine($"Highest Visibility: {currentStationCandidate.Item2} at Asteroid: {currentStationCandidate.Item1}");
            return currentStationCandidate;
        }
    }

    public static class PointFunctions
    {
        public static double DistanceTo(this (int, int) point1, (int, int) point2)
        {
            var a = (double)(point2.Item1 - point1.Item1);
            var b = (double)(point2.Item2 - point1.Item2);

            return Math.Sqrt(a * a + b * b);
        }

        public static int GreatestCommonDenominator(this (int, int) ratio) 
        {
            return ratio.Item2 == 0 ? Math.Abs(ratio.Item1) : GreatestCommonDenominator((ratio.Item2, ratio.Item1 % ratio.Item2));
        }

        public static double Angle(this (int, int) relativePoint, (int, int) point)
        {
            var v = point.Subtract(relativePoint);
            var angle = Math.Atan2(v.Item2, v.Item1) * (180.0 / Math.PI);
            angle += 90.0;
            if (angle < 0f)
            {
                angle += 360f;
            }

            return angle;
        }

        public static (int, int) Subtract(this (int, int) a, (int, int) b)
        {
            return (a.Item1 - b.Item1, a.Item2 - b.Item2);
        }
    }
}