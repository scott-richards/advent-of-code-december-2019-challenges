﻿namespace AoC_2019_Day_6
{
    using System.IO;
    using System;
    using System.Collections.Generic;

    public class Program
    {
        static void Main(string[] args)
        {
            var orbitData = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "orbits.txt"));
            var orbits = orbitData[0].Split(')');
            var rootOrbit = new OrbitNode(orbits[0]);
            rootOrbit.Children.Add(new OrbitNode(orbits[1]));
            var orbitTree = new OrbitTree();
            var looseNodes = new List<OrbitNode>();
            foreach (var orbit in orbitData)
            {
                orbits = orbit.Split(')');
                var orbitedNode = looseNodes.Find(node => node.Name == orbits[0]) ?? CreateAndStoreNode(new OrbitNode(orbits[0]));
                var orbitingNode = looseNodes.Find(node => node.Name == orbits[1]) ?? CreateAndStoreNode(new OrbitNode(orbits[1]));
                orbitedNode.Children.Add(orbitingNode);
            }
            orbitTree.SetRoot(looseNodes.Find(node => node.Name == "COM"));
            Console.WriteLine(orbitTree.DirectOrbits());
            Console.WriteLine(orbitTree.IndirectOrbits());
            Console.WriteLine(orbitTree.GetOrbitalTransfersRequired(orbitTree.TryGet("YOU"), orbitTree.TryGet("SAN")));
            Console.ReadKey();

            OrbitNode CreateAndStoreNode(OrbitNode node)
            {
                looseNodes.Add(node);
                return node;
            }
        }
    }

    public class OrbitNode
    {
        public string Name { get; }

        private OrbitNode _parent;

        public List<OrbitNode> Children { get; set; }

        private int _depth;

        public OrbitNode(string name, OrbitNode parent = null)
        {
            Name = name;
            Children = new List<OrbitNode>();
            _parent = parent;
        }

        public OrbitNode TryGet(string name)
        {
            if (Name == name)
            {
                return this;
            }
            foreach (var nodeChild in Children)
            {
                var nodeChildTryGet = nodeChild.TryGet(name);
                if(nodeChildTryGet != null)
                {
                    return nodeChildTryGet;
                }
            }
            return null;
        }

        public OrbitNode TryGet(OrbitNode node)
        {
            if (this == node)
            {
                return this;
            }
            foreach (var nodeChild in Children)
            {
                var nodeChildTryGet = nodeChild.TryGet(node);
                if(nodeChildTryGet != null)
                {
                    return nodeChildTryGet;
                }
            }
            return null;
        }

        public int DirectOrbits()
        {
            var childDirectOrbits = 0;
            foreach (var child in Children)
            {
                childDirectOrbits += child.DirectOrbits();
            }
            return Children.Count + childDirectOrbits;
        }

        public int IndirectOrbits()
        {
            var indirectOrbits = 0;
            foreach (var child in Children)
            {
                indirectOrbits += child.IndirectOrbits();
                indirectOrbits += child.GetDepth() - 1;
            }
            return indirectOrbits;
        }

        public void SetDepth(int depth, OrbitNode parentNode)
        {
            _depth = depth;
            _parent = parentNode;
            foreach (var child in Children)
            {
                child.SetDepth(depth + 1, this);
            }
        }

        public int GetDepth()
        {
            return _depth;
        }

        public OrbitNode GetParent()
        {
            return _parent;
        }
    }

    public class OrbitTree
    {
        public OrbitNode Root { get; private set; }

        public OrbitNode TryGet(string name)
        {
            return Root?.TryGet(name);
        }

        public int DirectOrbits()
        {
            return Root.DirectOrbits();
        }

        public int IndirectOrbits()
        {
            return Root.IndirectOrbits();
        }

        public void SetRoot(OrbitNode root)
        {
            Root = root;
            Root.SetDepth(0, null);
        }

        public int GetOrbitalTransfersRequired(OrbitNode pointA, OrbitNode pointB)
        {
            var commonParent = pointA.GetParent();
            while (commonParent != null)
            {
                if (commonParent.TryGet(pointB) == null)
                {
                    commonParent = commonParent.GetParent();
                }
                else
                {
                    return pointA.GetDepth() - commonParent.GetDepth() - 1 + (pointB.GetDepth() - commonParent.GetDepth() - 1);
                }
            }
            return 0;
        }
    }
}

/*
 * --- Day 6: Universal Orbit Map ---
You've landed at the Universal Orbit Map facility on Mercury. Because navigation in space often involves transferring between orbits, the orbit maps here are useful for finding efficient routes between, for example, you and Santa. You download a map of the local orbits (your puzzle input).

Except for the universal Center of Mass (COM), every object in space is in orbit around exactly one other object. An orbit looks roughly like this:

                  \
                   \
                    |
                    |
AAA--> o            o <--BBB
                    |
                    |
                   /
                  /
In this diagram, the object BBB is in orbit around AAA. The path that BBB takes around AAA (drawn with lines) is only partly shown. In the map data, this orbital relationship is written AAA)BBB, which means "BBB is in orbit around AAA".

Before you use your map data to plot a course, you need to make sure it wasn't corrupted during the download. To verify maps, the Universal Orbit Map facility uses orbit count checksums - the total number of direct orbits (like the one shown above) and indirect orbits.

Whenever A orbits B and B orbits C, then A indirectly orbits C. This chain can be any number of objects long: if A orbits B, B orbits C, and C orbits D, then A indirectly orbits D.

For example, suppose you have the following map:

COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
Visually, the above map of orbits looks like this:

        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I
In this visual representation, when two objects are connected by a line, the one on the right directly orbits the one on the left.

Here, we can count the total number of orbits as follows:

D directly orbits C and indirectly orbits B and COM, a total of 3 orbits.
L directly orbits K and indirectly orbits J, E, D, C, B, and COM, a total of 7 orbits.
COM orbits nothing.
The total number of direct and indirect orbits in this example is 42.

What is the total number of direct and indirect orbits in your map data?

Your puzzle answer was 322508.

--- Part Two ---
Now, you just need to figure out how many orbital transfers you (YOU) need to take to get to Santa (SAN).

You start at the object YOU are orbiting; your destination is the object SAN is orbiting. An orbital transfer lets you move from any object to an object orbiting or orbited by that object.

For example, suppose you have the following map:

COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN
Visually, the above map of orbits looks like this:

                          YOU
                         /
        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I - SAN
In this example, YOU are in orbit around K, and SAN is in orbit around I. To move from K to I, a minimum of 4 orbital transfers are required:

K to J
J to E
E to D
D to I
Afterward, the map of orbits looks like this:

        G - H       J - K - L
       /           /
COM - B - C - D - E - F
               \
                I - SAN
                 \
                  YOU
What is the minimum number of orbital transfers required to move from the object YOU are orbiting to the object SAN is orbiting? (Between the objects they are orbiting - not between YOU and SAN.)

Your puzzle answer was 496.

Both parts of this puzzle are complete! They provide two gold stars: **

At this point, you should return to your Advent calendar and try another puzzle.

If you still want to see it, you can get your puzzle input.

You can also [Share] this puzzle.
*/