﻿namespace AoC_2019_Day_12
{
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            new MoonTracker().Track(1000);
        }
    }

    public class MoonTracker
    {
        private (int, int, int)[,] trackedMoons;

        public MoonTracker()
        {
            trackedMoons = new[,]
            {
                { (-1,-4,0), (0,0,0) },
                { (4,7,-1), (0,0,0) },
                { (-14,-10,9), (0,0,0) },
                { (1,2,17), (0,0,0) }
            };
        }

        private void Tick()
        {
            for (var currentMoon = 0; currentMoon < trackedMoons.GetLength(0); currentMoon++)
            {
                UpdateVelocity(currentMoon);
            }

            for (var currentMoon = 0; currentMoon < trackedMoons.GetLength(0); currentMoon++)
            {
                ApplyVelocity(currentMoon);
            }
        }

        public void Track(int step)
        {
            var trackedMoonsOriginal = SomeMethods.Copy(trackedMoons);
            for (var i = 0; i < step; i++)
            {
                Tick();
            }
            Console.WriteLine($"{TotalEnergy()}");

            trackedMoons = trackedMoonsOriginal;
            trackedMoonsOriginal = SomeMethods.Copy(trackedMoons);
            var axisMatches = 0;
            ulong t = 0;
            ulong xt = 0;
            ulong yt = 0;
            ulong zt = 0;
            while (axisMatches < 3)
            {
                t++;
                Tick();
                var xMatch = true;
                var yMatch = true;
                var zMatch = true;
                for (var i = 0; i < trackedMoons.GetLength(0); i++)
                {
                    if (xMatch && xt == 0 && !(trackedMoons[i, 0].Item1 == trackedMoonsOriginal[i, 0].Item1 && trackedMoons[i, 1].Item1 == 0))
                    {
                        xMatch = false;
                    }
                    if (yMatch && yt == 0 && !(trackedMoons[i, 0].Item2 == trackedMoonsOriginal[i, 0].Item2 && trackedMoons[i, 1].Item2 == 0))
                    {
                        yMatch = false;
                    }
                    if (zMatch && zt == 0 && !(trackedMoons[i, 0].Item3 == trackedMoonsOriginal[i, 0].Item3 && trackedMoons[i, 1].Item3 == 0))
                    {
                        zMatch = false;
                    }
                }

                if (xMatch && xt == 0)
                {
                    xt = t;
                    axisMatches++;
                }
                
                if (yMatch && yt == 0)
                {
                    yt = t;
                    axisMatches++;
                }
                
                if (zMatch && zt == 0)
                {
                    zt = t;
                    axisMatches++;
                }
            }

            var lcmXY = xt * yt / GreatestCommonDenominator(xt, yt);
            var lcmXYZ = lcmXY * zt / GreatestCommonDenominator(lcmXY, zt);
            Console.WriteLine($"{lcmXYZ}");
            Console.ReadLine();
        }
        private static ulong GreatestCommonDenominator(ulong a, ulong b) 
        {
            return b == 0 ? a : GreatestCommonDenominator(b, a % b);
        }

        private void UpdateVelocity(int moon)
        {
            for (var currentMoon = 0; currentMoon < trackedMoons.GetLength(0); currentMoon++)
            {
                if (currentMoon != moon)
                {
                    trackedMoons[moon, 1].Item1 += trackedMoons[moon, 0].Item1 < trackedMoons[currentMoon, 0].Item1 ? 1 : trackedMoons[moon, 0].Item1 > trackedMoons[currentMoon, 0].Item1 ? -1 : 0;
                    trackedMoons[moon, 1].Item2 += trackedMoons[moon, 0].Item2 < trackedMoons[currentMoon, 0].Item2 ? 1 : trackedMoons[moon, 0].Item2 > trackedMoons[currentMoon, 0].Item2 ? -1 : 0;
                    trackedMoons[moon, 1].Item3 += trackedMoons[moon, 0].Item3 < trackedMoons[currentMoon, 0].Item3 ? 1 : trackedMoons[moon, 0].Item3 > trackedMoons[currentMoon, 0].Item3 ? -1 : 0;
                }
            }
        }

        private void ApplyVelocity(int moon)
        {
            trackedMoons[moon, 0] = trackedMoons[moon, 0].Add(trackedMoons[moon, 1]);
        }

        private int TotalEnergy()
        {
            var totalEnergy = 0;
            for (var currentMoon = 0; currentMoon < trackedMoons.GetLength(0); currentMoon++)
            {
                totalEnergy += trackedMoons[currentMoon, 0].AbsoluteValuesSummed() * trackedMoons[currentMoon, 1].AbsoluteValuesSummed();
            }

            return totalEnergy;
        }
    }

    public static class SomeMethods
    {
        public static (int, int, int) Add(this (int, int, int) vector3IntA, (int, int, int) vector3IntB)
        {
            vector3IntA.Item1 += vector3IntB.Item1;
            vector3IntA.Item2 += vector3IntB.Item2;
            vector3IntA.Item3 += vector3IntB.Item3;
            return vector3IntA;
        }

        public static (int, int, int) AbsoluteValues(this (int, int, int) vector3Int)
        {
            return (Math.Abs(vector3Int.Item1), Math.Abs(vector3Int.Item2), Math.Abs(vector3Int.Item3));
        }

        public static int AbsoluteValuesSummed(this (int, int, int) vector3Int)
        {
            var absValuesVector3Int = vector3Int.AbsoluteValues();
            return absValuesVector3Int.Item1 + absValuesVector3Int.Item2 + absValuesVector3Int.Item3;
        }

        public static T[,] Copy<T>(T[,] arrayToCopy)
        {
            var array = new T[arrayToCopy.GetLength(0),arrayToCopy.GetLength(1)];
            for (var x = 0; x < arrayToCopy.GetLength(0); x++)
            {
                for (var y = 0; y < arrayToCopy.GetLength(1); y++)
                {
                    array[x, y] = arrayToCopy[x, y];
                }
            }

            return array;
        }
    }
}
