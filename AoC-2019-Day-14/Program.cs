﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC_2019_Day_14
{
    class Program
    {
        private static Dictionary<string, (Dictionary<string, int> Inputs, KeyValuePair<string, int> Output)> reactions;

        static void Main(string[] args)
        {
            SetReactions();
            //Part A
            Console.WriteLine(OreRequired(1));

            //Part B
            var x = 3;
            var y = 2;
            var target = 1_000_000_000_000;
            var fuelFloor = target / OreRequired(1);
            var fuelCeiling = fuelFloor * x / y;
            while (OreRequired(fuelCeiling) > target)
            {
                x++;
                y++;
                fuelCeiling = fuelFloor * x / y;
            }

            x--;
            y--;
            fuelCeiling = fuelFloor * x / y;

            var currentFuel = (fuelFloor + fuelCeiling) / 2;
            while (true)
            {
                if (OreRequired(currentFuel) < target)
                {
                    if (fuelFloor == currentFuel)
                    {
                        break;
                    }
                    fuelFloor = currentFuel;
                }
                else
                {
                    fuelCeiling = currentFuel;
                }
                currentFuel = (fuelFloor + fuelCeiling) / 2;
            }
            Console.WriteLine($"{fuelFloor}");
        }

        private static Int64 OreRequired(Int64 fuel)
        {
            var fuelReaction = new Dictionary<string, Int64> {{"FUEL", fuel}};
            while (fuelReaction.Any(pair => pair.Key != "ORE" && pair.Value > 0))
            {
                var current = fuelReaction.First(pair => pair.Key != "ORE" && pair.Value > 0);
                var reaction = reactions[current.Key];
                var times = current.Value / reaction.Output.Value;
                times = times > 0 ? times : 1;
                fuelReaction[current.Key] -= reaction.Output.Value * times;
                foreach (var input in reaction.Inputs)
                {
                    if (fuelReaction.ContainsKey(input.Key))
                    {
                        fuelReaction[input.Key] += input.Value* times;
                    }
                    else
                    {
                        fuelReaction.Add(input.Key, input.Value* times);
                    }
                }
            }

            return fuelReaction["ORE"];
        }

        private static void SetReactions()
        {
            reactions = new Dictionary<string, (Dictionary<string, int> Inputs, KeyValuePair<string, int> Output)>();
            var reactionsInput = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt"));
            foreach (var line in reactionsInput)
            {
                var inputOutputSplit = line.Split(" => ");
                var outputSplit = inputOutputSplit[1].Trim().Split(" ");
                var outputType = outputSplit[1];
                var outputAmount = int.Parse(outputSplit[0]);
                var inputsSplit = inputOutputSplit[0].Split(", ");
                reactions.Add(outputType, (new Dictionary<string, int>(), new KeyValuePair<string, int>(outputType, outputAmount)));
                foreach (var input in inputsSplit)
                {
                    var inputSplit = input.Split(" ");
                    var inputType = inputSplit[1];
                    var inputAmount = int.Parse(inputSplit[0]);
                    reactions[outputType].Inputs.Add(inputType, inputAmount);
                }
            }
        }
    }
}
