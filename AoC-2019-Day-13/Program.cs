﻿namespace AoC_2019
{
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System;

    class Program
    {
        private static void Main(string[] args)
        {
            Console.CursorVisible = false;
            var codes = new Hashtable();
            var program = Array.ConvertAll(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt")).Split(','), long.Parse);
            for (var i = 0; i < program.Length; i++)
            {
                codes.Add(i, program[i]);
            }
            new ArcadeGame(codes);
            Console.ReadKey();
        }
    }

    public class ArcadeGame : IInput, IOutput, IHalt
    {
        private Dictionary<(int, int), int> _tiles = new Dictionary<(int, int), int>();
        private (int xPos, int yPos, int xDirection, int yDirection) _ballPositionAndDirection;
        private (int, int) _paddlePosition;
        private (int, int) _currentOutputPosition;
        private int _outputCounter;
        private int _score;

        public ArcadeGame(Hashtable paintProgram)
        {
            var intCodeComputer = new IntCodeComputer(paintProgram, this, this, this);
            intCodeComputer.Run();
        }

        public void PutOutput(long output)
        {
            if (_outputCounter == 0)
            {
                _currentOutputPosition.Item1 = (int) output;
            }
            else if (_outputCounter == 1)
            {
                _currentOutputPosition.Item2 = (int) output;
            }
            else if (_outputCounter == 2)
            {
                if (_currentOutputPosition.Item1 == -1 && _currentOutputPosition.Item2 == 0)
                {
                    _score = (int) output;
                }
                else
                {
                    if (!_tiles.ContainsKey(_currentOutputPosition))
                    {
                        _tiles.Add(_currentOutputPosition, (int) output);
                    }
                    else
                    {
                        _tiles[_currentOutputPosition] = (int) output;
                    }

                    if ((int) output == 4)
                    {
                        _ballPositionAndDirection = (_currentOutputPosition.Item1, _currentOutputPosition.Item2, _currentOutputPosition.Item1 > _ballPositionAndDirection.Item1 ? 1 : -1,  _currentOutputPosition.Item2 < _ballPositionAndDirection.Item2 ? 1 : -1);
                    }
                    if ((int) output == 3)
                    {
                        _paddlePosition = (_currentOutputPosition.Item1, _currentOutputPosition.Item2);
                    }
                }
            }

            _outputCounter++;
            if (_outputCounter > 2)
            {
                _outputCounter = 0;
            }
        }

        public void Halt()
        {
            DrawBoardState();
        }

        public int GetInput()
        {
            DrawBoardState();
            System.Threading.Thread.Sleep(5);
            return CheatMode();
            //return GetJoystickInput(Console.ReadKey());
        }

        private int CheatMode()
        {
            int direction;
            if (_paddlePosition.Item1 == _ballPositionAndDirection.Item1)
            {
                direction = _ballPositionAndDirection.Item4 == 1 ? 0 : _ballPositionAndDirection.Item3;
            }
            else if (_paddlePosition.Item1 == _ballPositionAndDirection.Item1 - 1 && _ballPositionAndDirection.Item3 == -1 ||
                     _paddlePosition.Item1 == _ballPositionAndDirection.Item1 + 1 && _ballPositionAndDirection.Item3 == 1)
            {
                direction = 0;
            }
            else
            {
                direction = _paddlePosition.Item1 < _ballPositionAndDirection.Item1 ? 1 : -1;
            }

            return direction;
        }

        private int GetJoystickInput(ConsoleKeyInfo key)
        {
            switch (key.Key)
            {
                case ConsoleKey.LeftArrow:
                {
                    return -1;
                }

                case ConsoleKey.RightArrow:
                {
                    return 1;
                }
                    
                default:
                    return 0;
            }
        }

        private void DrawBoardState()
        {
            var yMax = 0;
            var xMax = 0;
            foreach (var tilesKey in _tiles.Keys)
            {
                if (tilesKey.Item1 > xMax)
                {
                    xMax = tilesKey.Item1;
                }
                if (tilesKey.Item2 > yMax)
                {
                    yMax = tilesKey.Item2;
                }
            }
            Console.Clear();
            for (var y = 0; y <= yMax; y++)
            {
                for (var x = 0; x <= xMax; x++)
                {
                    if (_tiles.ContainsKey((x, y)))
                    {
                        Console.Write(DrawTile(_tiles[(x,y)]));
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Score: {_score}");
        }

        private char DrawTile(int tile)
        {
            switch (tile)
            {
                case 0:
                    return ' ';
                case 1:
                    return '#';
                case 2:
                    return '*';
                case 3:
                    return '=';
                case 4:
                    return '.';
            }

            return ' ';
        }
    }

    public interface IOutput
    {
        void PutOutput(long output);
    }

    public interface IInput
    {
        int GetInput();
    }

    public interface IHalt
    {
        void Halt();
    }
}