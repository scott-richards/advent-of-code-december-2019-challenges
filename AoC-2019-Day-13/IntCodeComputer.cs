﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace AoC_2019
{
    public class IntCodeComputer : IInput, IOutput, IHalt
    {
        private Hashtable _codes;
        private IOutput _output;
        private IInput _input;
        private IHalt _halt;
        private long _relativeBase = 0;

        public IntCodeComputer(Hashtable codes, IInput input, IOutput output = null, IHalt halt = null)
        {
            _codes = codes;
            _output = output ?? this;
            _input = input ?? this;
            _halt = halt ?? this;
        }

        public long Run()
        {
            int step;
            int currentCode;
            for (var i = 0; i < _codes.Count; i += step)
            {
                currentCode = (int) (long) _codes[i];
                if (currentCode % 100 == 1)
                {
                    OpCodeAdd(i, currentCode / 100 % 10, currentCode / 1000 % 10, currentCode / 10000 % 10);
                    step = 4;
                }
                else if (currentCode % 100 == 2)
                {
                    OpCodeMultiply(i, currentCode / 100 % 10, currentCode / 1000 % 10, currentCode / 10000 % 10);
                    step = 4;
                }
                else if (currentCode % 100 == 3)
                {
                    var input = _input.GetInput();
                    OpCodeInput(i, input, currentCode / 100 % 10);
                    step = 2;
                }
                else if (currentCode % 100 == 4)
                {
                    OpCodeOutput(i, currentCode / 100 % 10);
                    step = 2;
                }
                else if (currentCode % 100 == 5)
                {
                    i = OpCodeJumpIfTrue(i, currentCode / 100 % 10, currentCode / 1000 % 10);
                    step = 0;
                }
                else if (currentCode % 100 == 6)
                {
                    i = OpCodeJumpIfFalse(i, currentCode / 100 % 10, currentCode / 1000 % 10);
                    step = 0;
                }
                else if (currentCode % 100 == 7)
                {
                    OpCodeLessThan(i, currentCode / 100 % 10, currentCode / 1000 % 10, currentCode / 10000 % 10);
                    step = 4;
                }
                else if (currentCode % 100 == 8)
                {
                    OpCodeEquals(i, currentCode / 100 % 10, currentCode / 1000 % 10, currentCode / 10000 % 10);
                    step = 4;
                }
                else if (currentCode % 100 == 9)
                {
                    OpCodeRelativeBaseModify(i, currentCode / 100 % 10);
                    step = 2;
                }
                else if (currentCode % 100 == 99)
                {
                    OpCodeHalt();
                    _halt.Halt();
                    break;
                }
                else
                {
                    throw new Exception($"Invalid op code, {_codes[i]}, read at position {i}.");
                }
            }

            return (long) _codes[0];
        }

        private void OpCodeMultiply(int index, int firstParameterMode, int secondParameterMode, int thirdParameterMode)
        {
            SetValue(thirdParameterMode, index + 3, GetValue(firstParameterMode, index + 1) * GetValue(secondParameterMode, index + 2));
        }

        private void OpCodeAdd(int index, int firstParameterMode, int secondParameterMode, int thirdParameterMode)
        {
            SetValue(thirdParameterMode, index + 3, GetValue(firstParameterMode, index + 1) + GetValue(secondParameterMode, index + 2));
        }

        private void OpCodeInput(int index, int input, int firstParameterMode)
        {
            SetValue(firstParameterMode, index + 1, input);
        }

        private void OpCodeOutput(int index, int firstParameterMode)
        {
            _output.PutOutput(GetValue(firstParameterMode, index + 1));
        }

        private int OpCodeJumpIfTrue(int index, int firstParameterMode, int secondParameterMode)
        {
            if (GetValue(firstParameterMode, index + 1) != 0)
            {
                return (int) GetValue(secondParameterMode, index + 2);
            }

            return index + 3;
        }

        private int OpCodeJumpIfFalse(int index, int firstParameterMode, int secondParameterMode)
        {
            if (GetValue(firstParameterMode, index + 1) == 0)
            {
                return (int) GetValue(secondParameterMode, index + 2);
            }

            return index + 3;
        }

        private void OpCodeLessThan(int index, int firstParameterMode, int secondParameterMode, int thirdParameterMode)
        {
            SetValue(thirdParameterMode, index + 3, GetValue(firstParameterMode, index + 1) < GetValue(secondParameterMode, index + 2) ? 1 : 0);
        }

        private void OpCodeEquals(int index, int firstParameterMode, int secondParameterMode, int thirdParameterMode)
        {
            SetValue(thirdParameterMode, index + 3, GetValue(firstParameterMode, index + 1) == GetValue(secondParameterMode, index + 2) ? 1 : 0);
        }

        private void OpCodeHalt()
        {
            Console.WriteLine("Halt.");
        }

        private void OpCodeRelativeBaseModify(int index, int firstParameterMode)
        {
            _relativeBase += GetValue(firstParameterMode, index + 1);
        }

        public long GetValue(int parameterMode, int index)
        {
            var finalIndex = parameterMode == 0 ? (int) (long) _codes[index] : parameterMode == 1 ? index : (int) (long) _codes[index] + (int) _relativeBase;
            if (!_codes.ContainsKey(finalIndex))
            {
                _codes.Add(finalIndex, (long) 0);
            }

            return (long) _codes[finalIndex];
        }

        public void SetValue(int parameterMode, int index, long value)
        {
            var finalIndex = parameterMode == 0 ? (int) (long) _codes[index] : parameterMode == 1 ? index : (int) (long) _codes[index] + (int) _relativeBase;
            if (!_codes.ContainsKey(finalIndex))
            {
                _codes.Add(finalIndex, value);
            }
            else
            {
                _codes[finalIndex] = value;
            }
        }

        public void PutOutput(long output)
        {
            Console.WriteLine($"Output: {output}");
        }

        public int GetInput()
        {
            return default;
        }

        public void Halt()
        {
        }
    }
}