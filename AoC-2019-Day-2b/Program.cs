﻿namespace AoC_2019
{
    using System.IO;
    using System;

    class Program
    {
        private static void Main(string[] args)
        {
            var codes = Array.ConvertAll(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt")).Split(','), int.Parse);
            for (var x = 0; x <= 99; x++)
            {
                for (var y = 0; y <= 99; y++)
                {
                    if (Output(codes.BlockCopy(), x, y) == 19690720)
                    {
                        Console.WriteLine($"100 * noun: {x} + verb: {y} = {100 * x + y}");
                    }
                }
            }
            Console.ReadKey();
        }

        private static int Output(int[] program, int addressOneValue, int addressTwoValue)
        {
            program[1] = addressOneValue;
            program[2] = addressTwoValue;
            for (var i = 0; i < program.Length; i += 4)
            {
                if (program[i] == 1)
                {
                    program[program[i + 3]] = program[program[i + 1]] + program[program[i + 2]];
                }
                else if (program[i] == 2)
                {
                    program[program[i + 3]] = program[program[i + 1]] * program[program[i + 2]];
                }
                else if (program[i] == 99)
                {
                    break;
                }
                else
                {
                    throw new Exception($"Invalid op code, {program[i]}, read at position {i}.");
                }
            }

            return program[0];
        }
    }

    public static class ExtensionMethods
    {
        public static int[] BlockCopy(this int[] array)
        {
            var copy = new int[array.Length];
            Buffer.BlockCopy(array, 0, copy, 0, array.Length * sizeof(int));
            return copy;
        }
    }
}
/*
 --- Part Two ---
"Good, the new computer seems to be working correctly! Keep it nearby during this mission - you'll probably use it again. Real Intcode computers support many more features than your new one, but we'll let you know what they are as you need them."

"However, your current priority should be to complete your gravity assist around the Moon. For this mission to succeed, we should settle on some terminology for the parts you've already built."

Intcode programs are given as a list of integers; these values are used as the initial state for the computer's memory. When you run an Intcode program, make sure to start by initializing memory to the program's values. A position in memory is called an address (for example, the first value in memory is at "address 0").

Opcodes (like 1, 2, or 99) mark the beginning of an instruction. The values used immediately after an opcode, if any, are called the instruction's parameters. For example, in the instruction 1,2,3,4, 1 is the opcode; 2, 3, and 4 are the parameters. The instruction 99 contains only an opcode and has no parameters.

The address of the current instruction is called the instruction pointer; it starts at 0. After an instruction finishes, the instruction pointer increases by the number of values in the instruction; until you add more instructions to the computer, this is always 4 (1 opcode + 3 parameters) for the add and multiply instructions. (The halt instruction would increase the instruction pointer by 1, but it halts the program instead.)

"With terminology out of the way, we're ready to proceed. To complete the gravity assist, you need to determine what pair of inputs produces the output 19690720."

The inputs should still be provided to the program by replacing the values at addresses 1 and 2, just like before. In this program, the value placed in address 1 is called the noun, and the value placed in address 2 is called the verb. Each of the two input values will be between 0 and 99, inclusive.

Once the program has halted, its output is available at address 0, also just like before. Each time you try a pair of inputs, make sure you first reset the computer's memory to the values in the program (your puzzle input) - in other words, don't reuse memory from a previous attempt.

Find the input noun and verb that cause the program to produce the output 19690720. What is 100 * noun + verb? (For example, if noun=12 and verb=2, the answer would be 1202.)
*/