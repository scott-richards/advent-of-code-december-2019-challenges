﻿using System.Collections;
using System.Threading.Tasks;

namespace AoC_2019
{
    using System.IO;
    using System;

    class Program
    {
        private static void Main(string[] args)
        {
            var codes = new Hashtable();
            var program = Array.ConvertAll(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "codes.txt")).Split(','), long.Parse);
            for (var i = 0; i < program.Length; i++)
            {
                codes.Add(i, program[i]);
            }
            var intCodeComputer = new IntCodeComputer(codes, new TestInput());
            _ = intCodeComputer.Run();
            Console.ReadKey();
        }
    }

    public class TestInput : IInput
    {
        public async Task<int> GetInput()
        {
            return 2;
        }
    }

    public interface IOutput
    {
        void PutOutput(long output);
    }

    public interface IInput
    {
        Task<int> GetInput();
    }

    public static class ExtensionMethods
    {
        public static int[] BlockCopy(this int[] array)
        {
            var copy = new int[array.Length];
            Buffer.BlockCopy(array, 0, copy, 0, array.Length * sizeof(int));
            return copy;
        }
    }
}